package net.avcompris.examples.users3.web.it;

import static io.restassured.RestAssured.given;
import static net.avcompris.commons3.web.it.utils.JSONUtils.parseJSON;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;

import io.restassured.response.Response;
import net.avcompris.commons3.web.ApplicationError;
import net.avcompris.commons3.web.HealthCheck;
import net.avcompris.commons3.web.it.utils.AbstractWebTest;
import net.avcompris.commons3.web.it.utils.RestAssured;

@RestAssured("${users.baseURL}")
public class HealthCheckTest extends AbstractWebTest {

	@Test
	public void healthcheck_withNoAuthorization_returns_200_with_data() throws Exception {

		final Response response = given()

				.when().get("/healthcheck")

				.then()

				.log().body()

				.statusCode(200)

				.extract().response();

		final String correlationId = response.getHeader("Correlation-ID");

		assertNull(correlationId, "correlationId should be null");

		final HealthCheck healthCheck = parseJSON(response.asString(), HealthCheck.class);

		assertEquals("examples-users3", healthCheck.getComponentName());
		assertEquals(true, healthCheck.isOk());
		assertNull(healthCheck.getRuntimeDbStatus());
	}

	@Test
	public void healthcheck_withAuthorization_returns_200_with_data() throws Exception {

		final Response response = given()

				.header("Authorization", "toto")

				.when().get("/healthcheck")

				.then()

				.log().body()

				.statusCode(200)

				.extract().response();

		final String correlationId = response.getHeader("Correlation-ID");

		assertNull(correlationId, "correlationId should be null");

		final HealthCheck healthCheck = parseJSON(response.asString(), HealthCheck.class);

		assertEquals("examples-users3", healthCheck.getComponentName());
		assertEquals(true, healthCheck.isOk());
		assertNull(healthCheck.getRuntimeDbStatus());
	}

	@Test
	public void unknown_path_returns_404_with_error() throws Exception {

		final Response response = given()

				.header("Authorization", "toto")

				.when().get("/api/v1/blahblah")

				.then()

				.statusCode(404)

				.extract().response();

		final String correlationId = response.getHeader("Correlation-ID");

		assertNull(correlationId, "correlationId should be null");

		final ApplicationError error = parseJSON(response.asString(), ApplicationError.class);

		assertNotNull(error, "error should not be null");

		assertEquals(404, error.getStatusCode());
		assertEquals(correlationId, error.getCorrelationId());
		assertEquals(null, error.getType());
	}
}
