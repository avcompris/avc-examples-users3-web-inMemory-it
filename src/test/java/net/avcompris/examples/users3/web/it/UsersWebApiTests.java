package net.avcompris.examples.users3.web.it;

import java.util.stream.Stream;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.provider.Arguments;

import net.avcompris.commons3.api.tests.ApiTestsUtils;
import net.avcompris.commons3.api.tests.TestSpecParametersExtension;
import net.avcompris.commons3.api.tests.TestsSpec.TestSpec;
import net.avcompris.commons3.web.it.utils.AbstractWebApiTest;
import net.avcompris.commons3.web.it.utils.RestAssured;
import net.avcompris.examples.users3.api.UserInfo;
import net.avcompris.examples.users3.api.UsersInfo;

@ExtendWith(TestSpecParametersExtension.class)
@RestAssured("${users.baseURL}")
public class UsersWebApiTests extends AbstractWebApiTest {

	public UsersWebApiTests(final TestSpec spec) {

		super(spec, "toto");
	}

	public static Stream<Arguments> testSpecs() throws Exception {

		return ApiTestsUtils.testSpecs( //
				UserInfo.class, //
				UsersInfo.class);
	}
}
